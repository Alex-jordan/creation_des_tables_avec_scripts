<?php

use App\Mail\contactMessage;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| contains the "web" middleware group. Now create something great!
| routes are loaded by the RouteServiceProvider within a group which
|
*/


Route::get('/','PagesController@home');

Route::get('Simuler','PagesController@simuler' );

Route::get('Contacts','contactController@contact');


Route::post('Contacts','contactController@verification');

Route::post('Simuler','PagesController@aftersimulate' );


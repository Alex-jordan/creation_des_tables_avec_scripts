<!DOCTYPE html>
<html lang="en">

<head>


    <title>Simulateur de récolte</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8" />
    <meta name="keywords" content="Alex jordan" />
    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>

    <link rel="stylesheet" href="css/w3.css">
    <link rel="stylesheet" href="js/w3.js">
    <link href="css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
    <link href="css/style.css" type="text/css" rel="stylesheet" media="all">
    <link href="css/fontawesome-all.min.css" rel="stylesheet">
    <link href="./assets/css/now-ui-kit.css?v=1.2.0" rel="stylesheet" />
    <link rel="stylesheet" href="style1.css">
    <link href="//fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">


</head>

<body>
<!-- banner -->
<div class="banner" id="home">
    <!-- header -->
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-gradient-secondary pt-3"  id="na">
            <ul>
                <h1>
                    <a class="navbar-brand text-white" href="http://127.0.0.1:8000">
                        <i class="fab fa-laravel"></i> Agri-Simulator
                    </a>
                </h1>
            </ul>
            <button class="navbar-toggler ml-md-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-lg-auto text-center">
                    <li >
                        <div class="search-box">
                            <input class="search-txt" type="text" name="" placeholder="Type to search" id="box">
                            <a class="search-btn" href="#">
                                <i class="fas fa-search"></i>
                        </div>
                    </li>

                    <li class="nav-item   mr-3 mt-lg-0 mt-3">
                        <a class="nav-link " href="http://127.0.0.1:8000">Home<i class="w3-margin-left fa fa-home"></i><span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item mr-3 mt-lg-0 mt-3">
                        <a class="nav-link " href="Contacts">Nous Contacter  <span class="fa fa-envelope mr-2"></span></a>
                    </li>
                    <li>
                        <a class="nav-link " href="Simuler">Simuler<i class="fa fa-cog fa-spin" style="font-size:18px"></i>
                        </a>
                    </li>
                </ul>
            </div>

        </nav>
    </header>
    <div class="w3-container">
        <h2>Gallery d'Agri-simulator</h2>
        <p class="text-muted">Cliquez sur une image pour la visualiser en grand format.</p>
    </div>
    </br>
    <div class="w3-row-padding">
        <div class="w3-container w3-third">
            <img src="images/harvest-3679075.jpg" style="width:80%;cursor:pointer"
                 onclick="onClick(this)" class="w3-hover-opacity">
        </div>
        <div class="w3-container w3-third">
            <img src="images/cocoa-3317863.jpg" style="width:80%;cursor:pointer"
                 onclick="onClick(this)" class="w3-hover-opacity">
        </div>
        <div class="w3-container w3-third">
            <img src="images/fruit-3247447.jpg" style="width:80%;cursor:pointer"
                 onclick="onClick(this)" class="w3-hover-opacity">
        </div>
    </div>

    <div id="modal01" class="w3-modal" onclick="this.style.display='none'">
        <!--<span class="w3-button w3-hover-red w3-xlarge w3-display-topright">&times;</span>-->
        <div class="w3-modal-content w3-animate-zoom">
            <img id="img01" >
        </div>
    </div>

    <script>
        function onClick(element) {
            document.getElementById("img01").src = element.src;
            document.getElementById("modal01").style.display = "block";
        }
    </script>

    <?php echo $__env->yieldContent('content'); ?>

    </br></br> </br></br> </br></br></br> </br></br></br>
    <!-- footer -->
    <footer id="footer" class="text-sm-left text-center " >
        <div class="container py-4 py-sm-5">
            <h2>
                <a class="navbar-brand text-white" href="#">
                    <i class="fab fa-laravel"></i> Agri-Simulator
                </a>
            </h2>
            <div class="row py-sm-5 py-3">
                <div class="col-lg-2 col-md-4 col-sm-6">
                    <ul class="list-agileits">
                        <li>
                            <a href="http://127.0.0.1:8000" class="nav-link">
                                Home<i class="w3-margin-left fa fa-home"></i>
                            </a>
                        </li>
                        <li class="my-2">
                            <a href="Simuler" class="nav-link scroll">
                                Simuler <i class="fa fa-cog fa-spin" style="font-size:16px"></i></a>
                        </li>
                        <li>
                            <a href="Contacts" class="nav-link ">
                                Nous contacter  <span class="fa fa-envelope"></span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="offset-lg-5"></div>
                <div class="col-lg-3 col-md-4 footer-end-grid mt-md-0 mt-sm-5">
                    <div class="fv3-contact">
                        <span class="fas fa-phone mr-2"></span>
                        <p class="d-inline">
                            +237 656-55-09-25
                        </p>
                    </div>
                    <div class="fv3-contact">
                        <span class="fas fa-mobile mr-2"></span>
                        <p class="d-inline">
                            +237 650-03-49-87
                        </p>
                    </div>
                    <div class="fv3-contact">
                        <span class="fas fa-envelope-open mr-2"></span>
                        <p class="d-inline">
                            <a href="mailto:alextsague98@gmail.com">alextsague98@gmail.com</a>
                        </p>
                    </div>
                </div>
            </div>
            <hr>
            <div class="d-flex justify-content-between pt-sm-5   footer-bottom-cpy">
                <div class="cpy-right text-center">
                    <p>© 2019 3il & Jangolo Sarl. Tout droits reservés | Designed.
                    </p>
                </div>
                <div class="social-icons pb-md-0 pb-4">
                    <ul class="social-iconsv2 agileinfo text-center">
                        <li class="ml-lg-5">
                            <a href="facebook.com">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a href="twitter.com">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fab fa-google-plus-g"></i>
                            </a>
                        </li>
                        <li>
                            <a href="www.linkedin.com">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    <!-- debut-de-smooth-scrolling -->

    <script>
        jQuery(document).ready(function ($) {
            $(".scroll").click(function (event) {
                event.preventDefault();

                $('html,body').animate({
                    scrollTop: $(this.hash).offset().top
                }, 1000);
            });
        });
    </script>
    <!-- //fin-de-smooth-scrolling -->


    <!-- smooth-scrolling-of-move-up -->
    <script>
        $(document).ready(function () {
            /*
            var defaults = {
                containerID: 'toTop', // fading element id
                containerHoverID: 'toTopHover', // fading element hover id
                scrollSpeed: 1200,
                easingType: 'linear'
            };
            */

            $().UItoTop({
                easingType: 'easeOutQuart'
            });

        });
    </script>
    <!-- //smooth-scrolling-of-move-up -->

<?php echo e(HTML::style('css/bootstrap.min.css')); ?>

<?php echo e(HTML::script('js/bootstrap.min.js')); ?>

<?php echo e(HTML::script('js/jquery.min.js')); ?>

<?php echo e(HTML::script('js/SmoothScroll.min.js')); ?>

<?php echo e(HTML::script('js/move-top.js')); ?>

<?php echo e(HTML::script('js/easing.js')); ?>


</body>

</html><?php /**PATH C:\xampp\htdocs\PROJET_SIMULATION\resources\views/layouts/layout.blade.php ENDPATH**/ ?>
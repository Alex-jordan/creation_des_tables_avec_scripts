<!-- debut-de-smooth-scrolling -->

<script>
    jQuery(document).ready(function ($) {
        $(".scroll").click(function (event) {
            event.preventDefault();

            $('html,body').animate({
                scrollTop: $(this.hash).offset().top
            }, 1000);
        });
    });
</script>
<!-- //fin-de-smooth-scrolling -->


<!-- smooth-scrolling-of-move-up -->
<script>
    $(document).ready(function () {
        /*
        var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear'
        };
        */

        $().UItoTop({
            easingType: 'easeOutQuart'
        });

    });
</script>
<!-- //smooth-scrolling-of-move-up -->

<?php echo e(HTML::style('css/bootstrap.min.css')); ?>

<?php echo e(HTML::script('js/bootstrap.min.js')); ?>

<?php echo e(HTML::script('js/jquery.min.js')); ?>

<?php echo e(HTML::script('js/SmoothScroll.min.js')); ?>

<?php echo e(HTML::script('js/move-top.js')); ?>

<?php echo e(HTML::script('js/easing.js')); ?>


</body>

</html><?php /**PATH C:\xampp\htdocs\PROJET_SIMULATION\resources\views/Pages/SmoothScroll.blade.php ENDPATH**/ ?>
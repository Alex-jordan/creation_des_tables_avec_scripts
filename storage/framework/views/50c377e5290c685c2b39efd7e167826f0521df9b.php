<!-- footer -->
<footer id="footer" class="text-sm-left text-center " >
    <div class="container py-4 py-sm-5">
        <h2>
            <a class="navbar-brand text-white" href="#">
                <i class="fab fa-laravel"></i> Agri-Simulator
            </a>
        </h2>
        <div class="row py-sm-5 py-3">
            <div class="col-lg-2 col-md-4 col-sm-6">
                <ul class="list-agileits">
                    <li>
                        <a href="index.html" class="nav-link">
                            Home
                        </a>
                    </li>
                    <li class="my-2">
                        <a href="#register" class="nav-link scroll">
                            S'inscrire
                        </a>
                    </li>
                    <li>
                        <a href="#contact" class="nav-link scroll">
                            Nous contacter
                        </a>
                    </li>
                </ul>
            </div>
            <div class="offset-lg-5"></div>
            <div class="col-lg-3 col-md-4 footer-end-grid mt-md-0 mt-sm-5">
                <div class="fv3-contact">
                    <span class="fas fa-phone mr-2"></span>
                    <p class="d-inline">
                        +237 656-55-09-25
                    </p>
                </div>
                <div class="fv3-contact">
                    <span class="fas fa-mobile mr-2"></span>
                    <p class="d-inline">
                        +237 650-03-49-87
                    </p>
                </div>
                <div class="fv3-contact">
                    <span class="fas fa-envelope-open mr-2"></span>
                    <p class="d-inline">
                        <a href="mailto:example@email.com">alextsague98@gmail.com</a>
                    </p>
                </div>
            </div>
        </div>
        <hr>
        <div class="d-flex justify-content-between pt-sm-5   footer-bottom-cpy">
            <div class="cpy-right text-center">
                <p>© 2019 3il & Jangolo Sarl. Tous droits reservés | Design by alex-jordan.
                </p>
            </div>
            <div class="social-icons pb-md-0 pb-4">
                <ul class="social-iconsv2 agileinfo text-center">
                    <li class="ml-lg-5">
                        <a href="#">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fab fa-twitter"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fab fa-google-plus-g"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fab fa-linkedin-in"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer><?php /**PATH C:\xampp\htdocs\PROJET_SIMULATION\resources\views/Pages/footer.blade.php ENDPATH**/ ?>
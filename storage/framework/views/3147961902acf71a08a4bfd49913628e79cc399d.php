 <!-- contact -->
    <div class="contact-wthree" id="contact">
        <div class="container py-sm-5">
            <div class="row py-lg-5 py-4">
                <div class="col-lg-4">
                    <div class="title-wthree">
                        <h3 class="agile-title">
                            contact
                        </h3>
                        <span></span>
                    </div>
                    <div class="d-sm-flex">
                        <a class="btn btn-primary mt-lg-5 mt-3 agile-link-cnt scroll" href="#contact" role="button">email Nous</a>
                        <a class="btn btn-primary mt-lg-5 mt-3 ml-4 agile-link-cnt bg-dark scroll" href="#footer">appelez nous</a>
                    </div>
                </div>
                <div class="offset-2"></div>
                <div class="col-lg-6 mt-lg-0 mt-5">
                    <!-- register form grid -->
                    <div class="register-top1">
                        <form action="#" method="get" class="register-wthree">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2 d-md-flex align-items-end justify-content-end px-md-0">
                                        <label class="mb-0">
                                            <span class="fas fa-user"></span>
                                        </label>
                                    </div>
                                    <div class="col-md-5">
                                        <label>
                                            First name
                                        </label>
                                        <input class="form-control" type="text" placeholder="alex" name="email" required="">
                                    </div>
                                    <div class="col-md-5">
                                        <label>
                                            Last name
                                        </label>
                                        <input class="form-control" type="text" placeholder="Tsague" name="email" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2 d-md-flex align-items-end justify-content-end px-md-0">
                                        <label class="mb-0">
                                            <span class="fas fa-envelope-open"></span>
                                        </label>
                                    </div>
                                    <div class="col-md-10">
                                        <label>
                                            Email
                                        </label>
                                        <input class="form-control" type="email" placeholder="example@email.com" name="email" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2 d-md-flex align-items-end justify-content-end px-md-0">
                                        <label class="mb-0">
                                            <span class="far fa-edit"></span>
                                        </label>
                                    </div>
                                    <div class="col-md-10">
                                        <label>
                                            Votre message
                                        </label>
                                        <textarea placeholder="Tapez votre message Ici" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-lg-5 mt-3">
                                <div class="offset-2"></div>
                                <div class="col-md-10">
                                    <button type="submit" class="btn btn-agile btn-block w-100">Envoyer</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
<?php /**PATH C:\xampp\htdocs\PROJET_SIMULATION\resources\views/contacts.blade.php ENDPATH**/ ?>
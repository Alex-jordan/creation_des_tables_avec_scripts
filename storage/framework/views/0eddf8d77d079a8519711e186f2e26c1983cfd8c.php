<?php $__env->startSection('content2'); ?>
<div class="container">
    <div class="row">

        <div class="form-div col-md-3 col-xs-12">

            <form method="POST" action="Simuler">
                <?php echo e(csrf_field()); ?>

                <div class="title is-5 has-text-primary mt-4">Agri-Simulator</div>

                <div class="form-group">
                    <select name="product" value="" id="product_id"
                            class="form-control form-contol-md" required>
                        <option value="">selectionner un produit...</option>
                        <option class="form-control" value="cacao">Cacao</option>
                        <option class="form-control">Arachide</option>
                        <option class="form-control">Chou blanc</option>
                        <option class="form-control">Concombre</option>
                        <option class="form-control">Maïs</option>
                        <option class="form-control">Manioc</option>
                        <option class="form-control">Pastèque</option>
                        <option class="form-control">Piment</option>
                        <option class="form-control">Plantain</option>
                        <option class="form-control">Tomates</option>
                    </select>
                </div>

                <div class="form-group" id="superficie">
                    <div class="input-group">
                        <input type="number" step="0.1" name="superficie" class="form-control "
                               value="" placeholder="Surface"
                               required>
                        <div class="input-group-append">
                            <div class="input-group-text">m²</div>
                        </div>
                    </div>
                </div>

                <div class="form-group" id="but">
                    <button type="submit" class="btn btn-primary  btn-md" id="b">
                        Simuler
                    </button>
                </div>
                
            </form>


        </div>
        <?php if(isset($result)): ?>
        <div class="result-div col-md-9 col-xs-12">
            <div class="content p-4">
           <h3 id="ent"> <b> <?php echo e($result['produit']); ?></b>  |  <b><?php echo e($result['superficie']); ?> m²</b></h3>
            <hr id="titre">
                <h5 id="ent">Description</h5>
                <?php echo e($file['description']); ?>

               
              <table class="w3-table w3-bordered w3-hoverable ">
                   
                        <tr>
                            <th >Ecartement</th>
                            <th >Prix du plan</th>
                            <th >Prix du Kg de Cacao</th>
                            <th >Densité /ha</th>
                        </tr>
                   
                   
                       <tr >
                            <td ><?php echo e($file['espacement']); ?></td>
                            <td><?php echo e($file['prix_plant']); ?> <?php echo e($file['devise']); ?></td>
                            <td><?php echo e($file['prix_kg_cacao']); ?> <?php echo e($file['devise']); ?></td>
                            <td><?php echo e($file['densite']); ?></td>
                            <td></td>
                        </tr>
                   
               </table>
               <br/>
               <center> <h2 id="ente">Simulations </h2> </center>
            <div class="content" id="simulation">
                <table class="w3-table w3-bordered w3-hoverable ">
                   
                        <tr>
                            <th >Nombre de plans à prévoir</th>
                            <th >Coût total des plans à prévoir ( 200Fcfa/plans )</th>
                        </tr>
                   
                   
                       <tr >
                            <td><?php echo e($result['nbre_plant']); ?> plans</td>
                            <td><?php echo e($result['cout_plant']); ?> <?php echo e($file['devise']); ?></td>
                        </tr>
                   
               </table>   
               
            </div>
            <b>Nota bene : </b>Pour votre superficie de <b><?php echo e($result['superficie']); ?> m²</b>, vous pourrez prévoir environ <b><?php echo e($result['nbre_plant']); ?></b> plans ce qui vous reviendrait à <b><?php echo e($result['cout_plant']); ?></b> Fcfa pour votre projet agricole.
            <div>
                <table class="w3-table w3-bordered w3-hoverable ">
                   
                        <tr>
                            <th >Années </th>
                            <th >Quantités produite</th>
                            <th >Rendements en Fcfa</th>
                        </tr>
                       <tr>
                            <td>4ème année</td>
                            <td><?php echo e($result['nkg4']); ?>Kg</td>
                            <td><?php echo e($result['rd4']); ?> <?php echo e($file['devise']); ?></td>
                        </tr>
                        <tr >
                            <td>5ème année</td>
                            <td><?php echo e($result['nkg5']); ?>Kg</td>
                            <td><?php echo e($result['rd5']); ?> <?php echo e($file['devise']); ?></td>
                        </tr>
                         <tr >
                            <td>6ème année</td>
                            <td><?php echo e($result['nkg6']); ?>Kg</td>
                            <td><?php echo e($result['rd6']); ?> <?php echo e($file['devise']); ?></td>
                        </tr>
               </table>   
               </div>  <br/>
                <h5 id="ent">Recommendations</h5>
                <ul>
                <?php $__currentLoopData = $file['recommendations']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $recommendation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><?php echo e($recommendation); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
                <hr>

                <h5 id="ent">A éviter !!!!!</h5>
                <ul>
                <?php $__currentLoopData = $file['fautes']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $a_eviter): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><?php echo e($a_eviter); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
                <hr>
                <h5 id="ent">A savoir</h5>
                <?php echo e($file['a_savoir']); ?>

            </div>
        </div>
        <?php endif; ?>
    </div>

</div>
<?php echo e(HTML::style('css/bootstrap.min.css')); ?>

<?php echo e(HTML::script('js/bootstrap.min.js')); ?>

<?php echo e(HTML::script('js/jquery.min.js')); ?>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.layout2', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\PROJET_SIMULATION\resources\views/Pages/Simuler.blade.php ENDPATH**/ ?>
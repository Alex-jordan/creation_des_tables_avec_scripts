 <!-- js -->
    <script src="js/jquery-2.2.3.min.js"></script>
 <!-- //js -->
    <!-- debut-script pour verification de password -->
    <script>
        window.onload = function () {
            document.getElementById("password1").onchange = validatePassword;
            document.getElementById("password2").onchange = validatePassword;
        }

        function validatePassword() {
            var pass2 = document.getElementById("password2").value;
            var pass1 = document.getElementById("password1").value;
            if (pass1 != pass2)
                document.getElementById("password2").setCustomValidity("Passwords Don't Match");
            else
                document.getElementById("password2").setCustomValidity('');
            
        }
    </script><?php /**PATH C:\xampp\htdocs\PROJET_SIMULATION\resources\views/Pages/verfification_mot_de_passe.blade.php ENDPATH**/ ?>
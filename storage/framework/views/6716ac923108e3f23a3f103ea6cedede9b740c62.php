<!DOCTYPE html>
<html lang="en">

<head>


    <title>Simulateur de récolte</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8" />
    <meta name="keywords" content="Alex jordan" />
    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>



    <link href="css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
    <link href="css/style.css" type="text/css" rel="stylesheet" media="all">
    <link href="css/fontawesome-all.min.css" rel="stylesheet">
    <link href="./assets/css/now-ui-kit.css?v=1.2.0" rel="stylesheet" />
    <link rel="stylesheet" href="style1.css">
    <link href="//fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">


</head>

<body>
<!-- banner -->
<div class="banner" id="home">
    <!-- header -->
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-gradient-secondary pt-3" >
            <h1>
                <a class="navbar-brand text-white" href="#">
                    <i class="fab fa-laravel"></i> Agri-Simulator
                </a>
            </h1>
            <h2><i class="fa fa-cog fa-spin"></i></h2>
            <button class="navbar-toggler ml-md-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-lg-auto text-center">
                    <li class="nav-item   mr-3 mt-lg-0 mt-3">
                        <a class="nav-link " href="http://127.0.0.1:8000">Home<span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item mr-3 mt-lg-0 mt-3">
                        <a class="nav-link " href="#contact">Contact</a>
                    </li>
                    <li>
                        <a class="nav-link " href="bootstrap">Simuler</a>

                    </li>
                </ul>
            </div>

        </nav>
    </header>
<!-- contact -->
<div class="contact-wthree" id="contact">
    <div class="container py-xs-5">
        <div class="row py-lg-5 py-4">
            <div class="col-lg-4">
                <div class="title-wthree">
                    <h3 class="agile-title">
                        contact
                    </h3>
                </div>
                <div class="d-sm-flex">
                    <a class="btn btn-primary mt-lg-5 mt-3 agile-link-cnt scroll" href="#contact" role="button">Ecrivez nous</a>
                    <a class="btn btn-primary mt-lg-5 mt-3 ml-4 agile-link-cnt bg-dark scroll" href="#footer">appelez nous</a>
                </div>
            </div>
            <div class="offset-2"></div>
            <div class="col-lg-6 mt-lg-0 mt-5">
                <!-- register form grid -->
                <div class="register-top1">
                    <form action="#" method="get" class="register-wthree">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2 d-md-flex align-items-end justify-content-end px-md-0">
                                    <label class="mb-0">
                                        <span class="fas fa-user"></span>
                                    </label>
                                </div>
                                <div class="col-md-5">
                                    <label>
                                        First name
                                    </label>
                                    <input class="form-control" type="text" placeholder="alex" name="text" required="">
                                </div>
                                <div class="col-md-5">
                                    <label>
                                        Last name
                                    </label>
                                    <input class="form-control" type="text" placeholder="Tsague" name="text" required="">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2 d-md-flex align-items-end justify-content-end px-md-0">
                                    <label class="mb-0">
                                        <span class="fas fa-envelope-open"></span>
                                    </label>
                                </div>
                                <div class="col-md-10">
                                    <label>
                                        Email *
                                    </label>
                                    <input class="form-control" type="email" placeholder="example@email.com" name="email" required="required">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2 d-md-flex align-items-end justify-content-end px-md-0">
                                    <label class="mb-0">
                                        <span class="far fa-edit"></span>
                                    </label>
                                </div>
                                <div class="col-md-10">
                                    <label>
                                        Votre message *
                                    </label>
                                    <textarea placeholder="Tapez votre message Ici" class="form-control" required="required"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-lg-5 mt-3">
                            <div class="offset-2"></div>
                            <div class="col-md-10">
                                <button type="submit" class="btn btn-primary btn-block w-100">Envoyer</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- footer -->
<footer id="footer" class="text-sm-left text-center " >
    <div class="container py-4 py-sm-5">
        <h2>
            <a class="navbar-brand text-white" href="#">
                <i class="fab fa-laravel"></i> Agri-Simulator
            </a>
        </h2>
        <div class="row py-sm-5 py-3">
            <div class="col-lg-2 col-md-4 col-sm-6">
                <ul class="list-agileits">
                    <li>
                        <a href="index.html" class="nav-link">
                            Home
                        </a>
                    </li>
                    <li class="my-2">
                        <a href="#register" class="nav-link scroll">
                            S'inscrire
                        </a>
                    </li>
                    <li>
                        <a href="#contact" class="nav-link scroll">
                            Nous contacter
                        </a>
                    </li>
                </ul>
            </div>
            <div class="offset-lg-5"></div>
            <div class="col-lg-3 col-md-4 footer-end-grid mt-md-0 mt-sm-5">
                <div class="fv3-contact">
                    <span class="fas fa-phone mr-2"></span>
                    <p class="d-inline">
                        +237 656-55-09-25
                    </p>
                </div>
                <div class="fv3-contact">
                    <span class="fas fa-mobile mr-2"></span>
                    <p class="d-inline">
                        +237 650-03-49-87
                    </p>
                </div>
                <div class="fv3-contact">
                    <span class="fas fa-envelope-open mr-2"></span>
                    <p class="d-inline">
                        <a href="mailto:example@email.com">alextsague98@gmail.com</a>
                    </p>
                </div>
            </div>
        </div>
        <hr>
        <div class="d-flex justify-content-between pt-sm-5   footer-bottom-cpy">
            <div class="cpy-right text-center">
                <p>© 2019 3il & Jangolo Sarl. Tous droits reservés | Design by alex-jordan.
                </p>
            </div>
            <div class="social-icons pb-md-0 pb-4">
                <ul class="social-iconsv2 agileinfo text-center">
                    <li class="ml-lg-5">
                        <a href="#">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fab fa-twitter"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fab fa-google-plus-g"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fab fa-linkedin-in"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<!-- debut-de-smooth-scrolling -->

<script>
    jQuery(document).ready(function ($) {
        $(".scroll").click(function (event) {
            event.preventDefault();

            $('html,body').animate({
                scrollTop: $(this.hash).offset().top
            }, 1000);
        });
    });
</script>
<!-- //fin-de-smooth-scrolling -->


<!-- smooth-scrolling-of-move-up -->
<script>
    $(document).ready(function () {
        /*
        var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear'
        };
        */

        $().UItoTop({
            easingType: 'easeOutQuart'
        });

    });
</script>
<!-- //smooth-scrolling-of-move-up -->

<?php echo e(HTML::style('css/bootstrap.min.css')); ?>

<?php echo e(HTML::script('js/bootstrap.min.js')); ?>

<?php echo e(HTML::script('js/jquery.min.js')); ?>

<?php echo e(HTML::script('js/SmoothScroll.min.js')); ?>

<?php echo e(HTML::script('js/move-top.js')); ?>

<?php echo e(HTML::script('js/easing.js')); ?>


</body>

</html><?php /**PATH C:\xampp\htdocs\PROJET_SIMULATION\resources\views/layout.blade.php ENDPATH**/ ?>
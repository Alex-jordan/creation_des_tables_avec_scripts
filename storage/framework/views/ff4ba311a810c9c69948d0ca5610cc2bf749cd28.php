<!--partners  -->
<div class="agileits-partners py-sm-5 py-4 clear-filter" filter-color="default" id="partners" >
    <div class="container py-lg-5">
        <div class="title-wthree text-center">
            <div id="demo" class="carousel slide" data-ride="carousel">

                <!-- Indicators -->
                <ul class="carousel-indicators">
                    <li data-target="#demo" data-slide-to="0" class="active"></li>
                    <li data-target="#demo" data-slide-to="1"></li>
                    <li data-target="#demo" data-slide-to="2"></li>  //*Alex Jordan code N°2
                </ul>

                <!-- The slideshow -->
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="/images/cocoa-3317863.jpg" alt="Los Angeles" width="500" height="300">
                    </div>
                    <div class="carousel-item">
                        <img src="/images/cocoa-3317863.jpg" alt="Chicago"width="500" height="300">
                    </div>
                    <div class="carousel-item">
                        <img src="/images/cocoa-3317863.jpg" alt="New York"width="500" height="300">
                    </div>
                </div>
                <!-- The slideshow -->

                <!-- Left and right controls -->
                <a class="carousel-control-prev" href="#demo" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#demo" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </a>

            </div>
        </div>

    </div>
</div>
<!-- //partners -->
    
    <?php /**PATH C:\xampp\htdocs\PROJET_SIMULATION\resources\views/Pages/Interface_simulateur.blade.php ENDPATH**/ ?>
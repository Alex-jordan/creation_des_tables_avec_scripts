<!-- Simuler -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"  id="exampleModalLabel" >Agri-Simulator</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/foo/bar" method="POST">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Produit</label>
                            <input type="text" class="form-control" placeholder="annanas" name="Name" id="recipient-name" required="required">
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-form-label">Superficie</label>
                            <input type="password" class="form-control" placeholder="m² " name="Password" id="password" required="">
                        </div>
                        <div class="right-w3l">
                            <input type="submit" class="form-control" value="Lancer">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--Simuler --><?php /**PATH C:\xampp\htdocs\PROJET_SIMULATION\resources\views/Pages/SIMULER.blade.php ENDPATH**/ ?>
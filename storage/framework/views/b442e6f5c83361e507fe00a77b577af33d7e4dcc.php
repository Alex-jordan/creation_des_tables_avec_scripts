<body>
<!-- banner -->
<div class="banner" id="home">
    <!-- header -->
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-gradient-secondary pt-3" >
            <h1>
                <a class="navbar-brand text-white" href="#">
                    <i class="fab fa-laravel"></i> Agri-Simulator
                </a>
            </h1>
            <h2><i class="fa fa-cog fa-spin"></i></h2>
            <button class="navbar-toggler ml-md-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-lg-auto text-center">
                    <li class="nav-item   mr-3 mt-lg-0 mt-3">
                        <a class="nav-link " href="http://127.0.0.1:8000">Home<span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item mr-3 mt-lg-0 mt-3">
                        <a class="nav-link " href="#contact">Contact</a>
                    </li>
                    <li>
                        <a class="nav-link " href="bootstrap">Simuler</a>

                    </li>
                </ul>
            </div>

        </nav>
    </header>
    <!-- //Entete -->
    <div class="container">

    </div>
</div><?php /**PATH C:\xampp\htdocs\PROJET_SIMULATION\resources\views/Pages/banner.blade.php ENDPATH**/ ?>
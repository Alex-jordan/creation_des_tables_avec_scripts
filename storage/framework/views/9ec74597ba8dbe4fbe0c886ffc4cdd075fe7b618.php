  <?php echo $__env->make('Pages.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

  <?php echo $__env->make('Pages.banner', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <?php echo $__env->make('Pages.connexion', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <?php echo $__env->make('Pages.verfification_mot_de_passe', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

 <!-- S'inscrire -->
    <div class="w3-register py-4" id="register">
        <div class="container py-lg-5">
            <div class="row register-form py-md-5">
                <!-- register details -->
                <div class="col-md-4 register-bottom d-flex flex-column register-right-w3ls">
                    <div class="title-wthree">
                        <h3 class="agile-title">
                            S'inscrire
                        </h3>
                        <span></span>
                    </div>

                    <p class="pb-sm-5">Bienvenue sur notre page de simulation vous ne serrez pas déçu .
                        Vous avez deux alternatives vous pouvez lancer une simulation sans toutefois vous inscrire ou vous pouvez aussi bien le faire après être inscrit pour qu'on puisse vous aider d'avantage dans votre domaine et vous informer sur toutes actualités agriculturales.</p>

                    <h5 class="pt-5 text-capitalize">Devenez un agriculteur android! 
                        <i class="far fa-hand-point-right"></i>
                    </h5>
                </div>
                <div class="offset-lg-2"></div>
                <div class="col-lg-6 wthree-form-left py-sm-5">
                    <!-- register form grid -->
                    <div class="register-top1">
                        <form action="/foo/bar" method="POST" class="register-wthree">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2 d-md-flex align-items-end justify-content-end px-md-0">
                                        <label class="mb-0">
                                            <span class="fas fa-user"></span>
                                        </label>
                                    </div>
                                    <div class="col-md-5">
                                        <label>
                                            First name
                                        </label>
                                        <input class="form-control" type="text" placeholder="Johnson" name="text" required="">
                                    </div>
                                    <div class="col-md-5">
                                        <label>
                                            Last name
                                        </label>
                                        <input class="form-control" type="text" placeholder="Kc" name="text" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2 d-md-flex align-items-end justify-content-end px-md-0">
                                        <label class="mb-0">
                                            <span class="fas fa-envelope-open"></span>
                                        </label>
                                    </div>
                                    <div class="col-md-10">
                                        <label>
                                            Email
                                        </label>
                                        <input class="form-control" type="email" placeholder="example@email.com" name="email" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2 d-md-flex align-items-end justify-content-end px-md-0">
                                        <label class="mb-0">
                                            <span class="fas fa-lock"></span>
                                        </label>
                                    </div>
                                    <div class="col-md-10">
                                        <label>
                                            password
                                        </label>
                                        <input type="password" class="form-control" placeholder="*******" name="Password" id="password1" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2 d-md-flex align-items-end justify-content-end px-md-0">
                                        <label class="mb-0">
                                            <span class="fas fa-lock"></span>
                                        </label>
                                    </div>
                                    <div class="col-md-10">
                                        <label>
                                            confirm password
                                        </label>
                                        <input type="password" class="form-control" placeholder="*******" name="Password" id="password2" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-lg-5 mt-3">
                                <div class="offset-2"></div>
                                <div class="col-md-10">
                                    <button type="submit" class="btn btn-primary">S'inscrire</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!--  //la fin de l'inscription est ici -->
                </div>

            </div>
            <!-- // les details de l'inscription container -->
        </div>
    </div>
    <!-- //Inscription -->



<?php echo $__env->make('Pages.SmoothScroll', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php echo $__env->make('Pages.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\PROJET_SIMULATION\resources\views/Inscription.blade.php ENDPATH**/ ?>
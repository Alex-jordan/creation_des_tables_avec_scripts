<!-- connexion -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title"  id="exampleModalLabel" >Se connecter</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/foo/bar" method="POST">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Nom d'utilisateur</label>
                            <input type="text" class="form-control" placeholder=" " name="Name" id="recipient-name" required="">
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-form-label">Mot de passe</label>
                            <input type="password" class="form-control" placeholder=" " name="Password" id="password" required="">
                        </div>
                        <div class="right-w3l">
                            <input type="submit" class="form-control" value="Login">
                        </div>
                        <div class="row sub-w3l my-3">
                            <div class="col sub-agile">
                                <input type="checkbox" id="brand1" value="">
                                <label for="brand1" class="text-secondary">
                                    <span></span>se rappeler de moi?</label>
                            </div>
                            <div class="col forgot-w3l text-right">
                                <a href="#" class="text-secondary">mot de passe oublié?</a>
                            </div>
                        </div>
                        <p class="text-center dont-do">vous n'avez pas de compte?
                            <a href="#register" class="scroll text-dark font-weight-bold">
                                S'inscrire maintenant</a>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- //connexion --><?php /**PATH C:\xampp\htdocs\PROJET_SIMULATION\resources\views/connexion.blade.php ENDPATH**/ ?>
@component('mail::message')
# Message provenant d'Agri-Simulator.

    - Nom_exp : {{ $nom }} {{ $prenom }}

    - Email_exp : {{ $email }}
 

@component('mail::panel')

    {{ $message }}

 @endcomponent

    Merci,
    {{ config('app.name') }}

@endcomponent

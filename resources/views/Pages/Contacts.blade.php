<!-- contact -->
@extends('layouts.layout2')


@section('content2')

    <div class="contact-wthree" id="contact">
        <div class="container py-xs-5">
            <div class="row py-lg-5 py-4">
                <div class="col-lg-4">
                    <div class="title-wthree">
                        <h3 class="agile-title">
                            contact<span class="fa fa-envelope "></span>
                        </h3>
                    </div>
                    <div class="d-sm-flex">
                        <a class="btn btn-primary mt-lg-5 mt-3 agile-link-cnt scroll" href="#contact" role="button">Ecrivez nous</a>
                        <a class="btn btn-primary mt-lg-5 mt-3 ml-4 agile-link-cnt bg-dark scroll" href="#footer">appelez nous</a>
                    </div>
                </div>
                <div class="offset-2"></div>
                <div class="col-lg-6 mt-lg-0 mt-5">
                    <!-- register form grid -->
                    <div class="register-top1">
                        <form action="Contacts" method="POST"  class="register-wthree" >
                            {{ csrf_field() }}
                            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }} ">
                                <div class="row">
                                    <div class="col-md-2 d-md-flex align-items-end justify-content-end px-md-0">
                                        <label class="mb-0">
                                            <span class="fas fa-user"></span>
                                        </label>
                                    </div>
                                    <div class="col-md-5">
                                        <label class="control-label" for="nom">
                                            First name :
                                        </label>
                                        <input class="form-control " type="text" placeholder="alex" id="nom" name="nom" required="required" value="{{ old('nom') }}">
                                        {!! $errors->first('nom','<span class="help-block">:message</span>') !!}
                                    </div>
                                    <div class="col-md-5">
                                        <label class="control-label" for="prenom">
                                            Last name :
                                        </label>
                                        <input class="form-control"  required="required" id="prenom" type="text" placeholder="Tsague" name="prenom" value="{{ old('prenom') }}">
                                        {!! $errors->first('prenom','<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2 d-md-flex align-items-end justify-content-end px-md-0">
                                        <label class="mb-0">
                                            <span class="fas fa-envelope-open"></span>
                                        </label>
                                    </div>
                                    <div class="col-md-10">
                                        <label class="control-label" for="email">
                                            Email :*
                                        </label>
                                        <input class="form-control " id="email" type="email" placeholder="example@email.com" name="email" required="required" value="{{ old('email') }}">
                                        {!! $errors->first('email','<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2 d-md-flex align-items-end justify-content-end px-md-0">
                                        <label class="mb-0">
                                            <span class="far fa-edit"></span>
                                        </label>
                                    </div>
                                    <div class="col-md-10">
                                        <label class="control-label" for="message">
                                            Votre message :*
                                        </label>
                                        <textarea placeholder="Tapez votre message Ici"  id="message" name="message" class="form-control" required="required" value="{{ old('message') }}"></textarea>
                                        {!! $errors->first('message','<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-lg-5 mt-3">
                                <div class="offset-2"></div>
                                <div class="col-md-10">
                                    <button type="submit" class="btn btn-primary btn-block w-100" name="mailform" value="Envoyer !"> Envoyer    <span class="fa fa-envelope"></span></button>
                                </div>

                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </br>
@endsection
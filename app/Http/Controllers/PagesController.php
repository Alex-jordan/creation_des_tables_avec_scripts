<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{


    public function home ()
    {
        return view('layouts.layout');

    }


    public function simuler ()
    {
        return view('Pages.Simuler');

    }

    public function aftersimulate (Request $request){

        //dd($request);
        $result = array();
        $produit = $request->input('product');

        $file = $this->getJsonfile($produit);
        
        $result["produit"] = $produit;
        $result["superficie"] = $request->input('superficie');

       $densite=1334; $prix_plant=200; $hectare=10000; $k4=500; $k5=700; $k6=800; $prix_kg_cacao=650; //*declaration des constantes

                     $nbre_plant = ($result["superficie"] * $densite) / $hectare ;
                     $cout_plant = $nbre_plant * $prix_plant ;
                     $nkg4 = ( $result["superficie"] * $k4 ) / $hectare ;
                     $nkg5 = ( $result["superficie"] * $k5 ) / $hectare ;
                     $nkg6 = ( $result["superficie"] * $k6 ) / $hectare ;
                         $rd4 = $nkg4 * $prix_kg_cacao ;
                         $rd5 = $nkg5 * $prix_kg_cacao ;                
                         $rd6 = $nkg6 * $prix_kg_cacao ;

            $result["cout_plant"] = $cout_plant;
            $result["nbre_plant"] = $nbre_plant;             
            $result["nkg4"] = $nkg4;
            $result["nkg5"] = $nkg5;
            $result["nkg6"] = $nkg6;
            $result["rd4"] = $rd4;          
            $result["rd5"] = $rd5;
            $result["rd6"] = $rd6;         
        return view('Pages.Simuler', compact(['result', 'file']));
    }

    public function getJsonfile($product){

        $path = storage_path().'\\' . $product .'.json'; // ie: /var/www/laravel/app/storage/cacao.json
        $json = json_decode(file_get_contents($path), true);
       return $json;
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\contactRequest;
use Illuminate\Support\Facades\Mail;
use App\Mail\contactMessage;

class contactController extends Controller
{

    public function contact ()
    {
        return view('Pages.Contacts');

    }

    public function verification (contactRequest $request)
    {
        $mailable = new contactMessage($request->nom,$request->prenom,$request->email,$request->message);
        Mail::to('alextsague98@gmail.com')->send($mailable);

        return  view('email.messageEnvoye');
    }
}
